public class Sorting {
    /**
     * Defines a method to generate an array of a given size and fill it with
     * random numbers up to a certain value
     * @param  Size The size of the array to generate
     * @return      Returns a randomised array
     */
    public static int[] GenerateArray(int Size, int Max) {
        int[] Values = new int[Size];
        for (int i = 0; i < Size; ++i) {
            Values[i] = (int) Math.floor(Math.random() * Max);
        }
        return Values;
    }

    /**
     * Defines a method to check whether an array is sorted or not
     * @param  Values The array to check
     * @return        Returns whether the array is sorted
     */
    public static boolean ArrayIsSorted(int[] Values) {
        for (int i = 0; i < Values.length - 1; ++i) {
            if (Values[i] > Values[i + 1]) return false;
        }
        return true;
    }

    /**
     * Defines a Bubble Sort algorithm that sorts an array
     * @param Values The array to sort
     */
    public static void BubbleSort(int[] Values) {
        for (int i = 0; i < Values.length; ++i) {
            boolean Sorted = true;

            for (int j = 0; j < Values.length - i - 1; ++j) {
                if (Values[j] > Values[j + 1]) {
                    int Temp = Values[j];
                    Values[j] = Values[j + 1];
                    Values[j + 1] = Temp;
                    Sorted = false;
                }
            }

            if (Sorted) {
                return;
            }
        }
    }

    /**
     * Defines an Insertion Sort algorithm that sorts an array
     * @param Values The array to sort
     */
    public static void InsertionSort(int[] Values) {
        for (int i = 0; i < Values.length; ++i) {
            for (int j = i; 0 < j; --j) {
                if (Values[j] < Values[j - 1]) {
                    int Temp = Values[j];
                    Values[j] = Values[j - 1];
                    Values[j - 1] = Temp;
                }
            }
        }
    }

    /**
     * Defines a Selection Sort algorithm that sorts an array
     * @param Values The array to sort 
     */
    public static void SelectionSort(int[] Values) {
        for (int i = 0; i < Values.length; ++i) {
            int LowestIndex = i;
            for (int j = i + 1; j < Values.length; ++j) {
                if (Values[j] < Values[LowestIndex]) LowestIndex = j;
            }
            int Temp = Values[i];
            Values[i] = Values[LowestIndex];
            Values[LowestIndex] = Temp;
        }
    }
}
