public class Searching {
    /**
     * Defines a Linear Search algorithm to go through a list and find the first
     * instance of Find
     * @param  Values The array to search through
     * @param  Find   The value to find
     * @return        Returns the index of the value, or -1 if not found
     */
    public static int LinearSearch(int[] Values, int Find) {
        for (int i = 0; i < Values.length; i++) {
            if (Values[i] == Find) return i;
        }

        return -1;
    }

    /**
     * Defines a Binary Search algorithm to search through a list and find the
     * index of Find
     * @param  Values The array to search through
     * @param  Find   The value to find
     * @return        Returns the index of the value, or -1 if not found
     */
    public static int BinarySearch(int[] Values, int Find) {
        int Low = 0, High = Values.length;

        while (Low < High) {
            int Middle = (Low + High) / 2;

            if (Values[Middle] == Find) {
                return Middle;
            }
            else if (Values[Middle] < Find) {
                Low = Middle + 1;
            }
            else {
                High = Middle - 1;
            }
        }

        return (Values[Low] == Find) ? Low : -1;
    }
}
