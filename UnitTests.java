public class UnitTests {
    public static void RunAll() {
        System.out.println("Linear Search works: " + TestLinearSearch());
        System.out.println("Binary Search works: " + TestBinarySearch());
        System.out.println("Bubble Sort works: " + TestBubbleSort());
        System.out.println("Insertion Sort works: " + TestInsertionSort());
        System.out.println("Selection Sort works: " + TestSelectionSort());
    }

    public static boolean TestLinearSearch() {
        int[] Values = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        for (int i = 0; i < Values.length; ++i) {
            if (Searching.LinearSearch(Values, Values[i]) != i) return false;
        }
        return true;
    }

    public static boolean TestBinarySearch() {
        int[] Values = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        for (int i = 0; i < Values.length; ++i) {
            if (Searching.BinarySearch(Values, Values[i]) != i) return false;
        }
        return true;
    }

    public static boolean TestBubbleSort() {
        int[] Values = Sorting.GenerateArray(50, 100);
        Sorting.BubbleSort(Values);
        return Sorting.ArrayIsSorted(Values);
    }

    public static boolean TestInsertionSort() {
        int[] Values = Sorting.GenerateArray(50, 100);
        Sorting.InsertionSort(Values);
        return Sorting.ArrayIsSorted(Values);
    }

    public static boolean TestSelectionSort() {
        int[] Values = Sorting.GenerateArray(50, 100);
        Sorting.SelectionSort(Values);
        return Sorting.ArrayIsSorted(Values);
    }
}
