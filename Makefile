JFLAGS := -g
JC := javac
.SUFFIXES: .java .class
.java.class:
	$(JC) $(JFLAGS) $*.java

CLASSES = \
	Main.java \
	UnitTests.java \
	Searching.java \
	Sorting.java

build: classes

classes: $(CLASSES:.java=.class)

run:
	java Main

clean:
	$(RM) *.class
